FROM golang:latest as builder
RUN mkdir -p /go/src/github.com/rafaelsq/proxy
WORKDIR /go/src/github.com/rafaelsq/proxy
COPY ./ ./
RUN CGO_ENABLED=0 go install ./

FROM scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /go/bin/proxy /
WORKDIR /
CMD ["/proxy"]
