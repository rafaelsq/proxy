build:
	docker build -t rafaelsq/proxy ./
run:
	docker run -it --rm -e TO="https://rafaelquintela.com/" -e PORT=80 -p 8080:80 rafaelsq/proxy
