package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"time"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	dest := os.Getenv("TO")
	if dest == "" {
		log.Fatal("env TO must be provided")
	}

	m := http.NewServeMux()
	m.HandleFunc("/", proxy(dest))

	srv := &http.Server{
		Addr:         fmt.Sprintf(":%s", port),
		Handler:      m,
		ReadTimeout:  time.Second * 2,
		WriteTimeout: time.Second * 5,
	}

	log.Printf("listen :%s\n", port)
	err := srv.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatal("ListenAndServeHTTP: ", err)
	}
}

func proxy(to string) func(http.ResponseWriter, *http.Request) {
	baseURL, err := url.Parse(to)
	if err != nil {
		log.Fatal(err)
	}

	return func(w http.ResponseWriter, r *http.Request) {
		proxy := httputil.NewSingleHostReverseProxy(&url.URL{
			Scheme: baseURL.Scheme,
			Host:   baseURL.Host,
		})

		req := r
		req.URL.Scheme = baseURL.Scheme
		req.URL.Host = baseURL.Host
		req.Host = baseURL.Host
		log.Println(req.URL)
		proxy.ServeHTTP(w, req)
	}
}
